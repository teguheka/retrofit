package com.teguh.retrofit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RetrofitExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(RetrofitExampleApplication.class, args);
	}

}
