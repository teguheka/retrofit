package com.teguh.retrofit.controller;

import com.teguh.retrofit.vendor.github.dto.GithubGistDTO;
import com.teguh.retrofit.vendor.github.dto.GithubContributorDTO;
import com.teguh.retrofit.vendor.github.service.GithubService;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executors;

import static org.springframework.http.MediaType.TEXT_EVENT_STREAM_VALUE;


@Slf4j
@RestController
@RequestMapping("/api/v1/github")
@RequiredArgsConstructor
public class GithubController {
    private final GithubService githubService;

    @GetMapping("/contributors")
    public ResponseEntity getContributors(@RequestParam(defaultValue = "teguheka") String owner,
                                          @RequestParam(defaultValue = "tax-service") String repo) throws IOException {
        Response<List<GithubContributorDTO>> response = githubService.contributors(owner, repo).execute();
        if (response.isSuccessful()) {
            return ResponseEntity.ok(response.body());
        } else {
            return ResponseEntity.status(HttpStatus.BAD_GATEWAY).body(response.raw().toString());
        }
    }

    @GetMapping("/gists")
    public ResponseEntity getGists(@RequestParam(defaultValue = "teguheka") String user) {
        Single<List<GithubGistDTO>> response = githubService.getGists(user);
        return ResponseEntity.ok(response.blockingGet());
    }

    @GetMapping("/statistics")
    public Single<String> getStatisticsForUser(@RequestParam("user") String user) {
        log.info("Getting GitHub statistics for user: {}", user);
        Scheduler scheduler = Schedulers.from(Executors.newFixedThreadPool(2));

        return Single.zip(
                githubService.getGists(user).subscribeOn(scheduler),
                githubService.getRepos(user).subscribeOn(scheduler),
                (gists, repos) -> "Gists: " + gists.size() + ", Repos: " + repos.size()
        );
    }

    @ApiOperation(
            value = "Get the total number of followers an repositories for all members of an organization",
            produces = TEXT_EVENT_STREAM_VALUE
    )
    @GetMapping("/organizations/{organization}/members-stats")
    public Single<String> getMembersStats(@PathVariable String organization) {
        log.info("Get the total number of followers an repositories for all members of an organization: {}", organization);
        Scheduler scheduler = Schedulers.from(Executors.newFixedThreadPool(2));

        return Single.zip(
                githubService.getOrganizationMembers(organization).subscribeOn(scheduler),
                (gists, repos) -> "Gists: " + gists.size() + ", Repos: " + repos.size()
        );
    }
}
