package com.teguh.retrofit.controller;

import com.teguh.retrofit.vendor.plantplaces.dto.PlantDTO;
import com.teguh.retrofit.vendor.plantplaces.service.PlantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/v1/plants")
public class PlantController {

    @Autowired private PlantService plantService;

    @GetMapping("")
    public List<PlantDTO> list(@RequestParam(defaultValue = "") String searchFilter) throws IOException {
        return plantService.fetch(searchFilter);
    }
}
