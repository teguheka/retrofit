package com.teguh.retrofit.vendor.plantplaces.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PlantDTO {
    @JsonProperty("id")
    private Long guid;
    @JsonProperty("genus")
    private String genus;
    @JsonProperty("species")
    private String species;
    @JsonProperty("cultivar")
    private String cultivar;
    @JsonProperty("common")
    private String common;
}
