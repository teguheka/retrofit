package com.teguh.retrofit.vendor.plantplaces.dao;

import com.teguh.retrofit.vendor.plantplaces.dto.RootPlantDTO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PlantDao {

    @GET("/perl/mobile/viewplantsjson.pl")
    Call<RootPlantDTO> findAll(@Query("Combined_Name") String combinedName);
}
