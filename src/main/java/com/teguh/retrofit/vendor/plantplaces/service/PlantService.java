package com.teguh.retrofit.vendor.plantplaces.service;

import com.teguh.retrofit.vendor.plantplaces.dao.PlantDao;
import com.teguh.retrofit.vendor.plantplaces.dto.PlantDTO;
import com.teguh.retrofit.vendor.plantplaces.dto.RootPlantDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.io.IOException;
import java.util.List;

@Service
public class PlantService {
    @Value("${custom.plantplaces.url}")
    private String plantplacesUrl;

    public List<PlantDTO> fetch(String searchFilter) throws IOException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(plantplacesUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
        PlantDao plantDao = retrofit.create(PlantDao.class);
        Call<RootPlantDTO> allPlants = plantDao.findAll(searchFilter);
        Response<RootPlantDTO> execute = allPlants.execute();
        RootPlantDTO rootPlantDTO = execute.body();
        return rootPlantDTO.getPlants();
    }
}
