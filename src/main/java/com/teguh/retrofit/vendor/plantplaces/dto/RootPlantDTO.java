package com.teguh.retrofit.vendor.plantplaces.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RootPlantDTO {
    @JsonProperty("plants")
    private List<PlantDTO> plants = null;
}
