package com.teguh.retrofit.vendor.github.service;

import com.teguh.retrofit.vendor.github.dto.GithubGistDTO;
import com.teguh.retrofit.vendor.github.dto.GithubRepositoryDTO;
import com.teguh.retrofit.vendor.github.dto.GithubContributorDTO;
import com.teguh.retrofit.vendor.github.dto.GithubUserDTO;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import java.util.List;

public interface GithubService {
    @GET("/users/{user}/gists")
    Single<List<GithubGistDTO>> getGists(@Path("user") String user);

    @GET("/users/{user}/repos")
    Single<List<GithubRepositoryDTO>> getRepos(@Path("user") String user);

    @GET("/repos/{owner}/{repo}/contributors")
    Call<List<GithubContributorDTO>> contributors(@Path("owner") String owner, @Path("repo") String repo);

    @GET("/users/{user}")
    Single<GithubUserDTO> getUser(@Path("user") String user);

    @GET("/orgs/{org}/members")
    Single<List<GithubUserDTO>> getOrganizationMembers(@Path("org") String org);

}
