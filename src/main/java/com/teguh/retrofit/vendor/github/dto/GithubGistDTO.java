package com.teguh.retrofit.vendor.github.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GithubGistDTO {
    private String id;
    private String url;
    @JsonProperty(value = "html_url")
    private String htmlUrl;
}