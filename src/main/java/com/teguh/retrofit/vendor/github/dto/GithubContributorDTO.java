package com.teguh.retrofit.vendor.github.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class GithubContributorDTO {
    private String login;
    private int contributions;
}
