package com.teguh.retrofit.vendor.github.service;

import com.teguh.retrofit.vendor.github.dto.GithubUserDTO;
import com.teguh.retrofit.vendor.github.dto.Stats;
import io.reactivex.Single;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GitHubOrganizationService {
    private final GithubService githubService;

    /*public Mono<String> getMembersStats(String org) {
        Mono<List<GithubUserDTO>> organizationMembers = githubService.getOrganizationMembers(org);
        return organizationMembers.flatMapMany(Flux::fromIterable)
                .flatMap(member -> githubService.getUser(member.getLogin()))
                .map(user -> new Stats<>(user.getFollowersNb(), user.getReposNumber()))
                .reduce(new Stats<>(0, 0), (x, y) -> new Stats<>(x.getFollowers() + y.getFollowers(), x.getRepositories() + y.getRepositories()))
                .map(Stats::toString);
    }

    public Flux<String> getMembers(String org) {
        Mono<List<GithubUserDTO>> organizationMembers = githubService.getOrganizationMembers(org);
        Flux<GithubUserDTO> users = organizationMembers.flatMapMany(Flux::fromIterable)
                .flatMap(member -> gitHubService.getUser(member.getLogin()));
        return users.map(user -> user.getLogin() + "(" + user.getName() + ")");
    }*/
}
